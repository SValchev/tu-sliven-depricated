import os

from django.db import models


def news_image_upload_location(instance, filename):
    return os.path.join(instance.id, filename)


class News(models.Model):
    class Meta:
        verbose_name_plural = 'News'
        verbose_name = 'News'

    title = models.CharField(max_length=1024)
    content = models.TextField()
    author = models.CharField(max_length=128)
    cover_image = models.ImageField(upload_to=news_image_upload_location)

    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)


class NewsImage(models.Model):
    news = models.ForeignKey(News)
    image = models.ImageField(upload_to=news_image_upload_location)
