from django.contrib import admin

# Register your models here.
from departments.models import DepartamentNews
from news.models import News, NewsImage


class DepartamentNewsTabularInline(admin.TabularInline):
    model = DepartamentNews


class NewsImagesTabularInline(admin.TabularInline):
    model = NewsImage
    extra = 3


class NewsAdmin(admin.ModelAdmin):
    inlines = (DepartamentNewsTabularInline, NewsImagesTabularInline,)


admin.site.register(News, NewsAdmin)
