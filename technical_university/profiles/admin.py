from django.contrib import admin

from profiles.models import Profile, TeacherProfile, Publication, StudentCouncilProfile, UserProfile

admin.site.register(Profile)
admin.site.register(TeacherProfile)
admin.site.register(StudentCouncilProfile)
admin.site.register(Publication)
admin.site.register(UserProfile)
