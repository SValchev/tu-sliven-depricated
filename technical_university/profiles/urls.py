from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        regex=r'^teacher/(?P<slug>[\w-]+)/$',
        view=views.TeacherProfileDetailView.as_view(),
        name='teacher_detail',
    ),
    url(
        regex=r'^teacher/(?P<slug>[\w-]+)/edit/$',
        view=views.TeacherProfileUpdateView.as_view(),
        name='teacher_edit',
    ),
    url(
        regex=r'^teacher/(?P<slug>[\w-]+)/publication/edit/$',
        view=views.PublicationUpdateView.as_view(),
        name='publication_edit',
    ),
    url(
        regex=r'^student-council/(?P<slug>[\w-]+)/$',
        view=views.StudentCouncilProfileDetailView.as_view(),
        name='student_council_detail',
    ),
    url(
        regex=r'^student-council/(?P<slug>[\w-]+)/edit/$',
        view=views.StudentCouncilProfileUpdateView.as_view(),
        name='student_council_edit',
    ),

]
