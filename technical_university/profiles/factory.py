from faker import Faker
from random import randint

"""
publishers = models.CharField(max_length=225)
    title = models.CharField(max_length=225)
    journal = models.CharField(max_length=225)
    publication_year = models.PositiveIntegerField(choices=YEAR_CHOICES)

"""


def get_publication_data():
    faker = Faker()
    publishers = ' '.join([faker.name() for x in range(3)])
    title = faker.text(max_nb_chars=225)
    journal = faker.text(max_nb_chars=225)
    publication_year = randint(2000, 2014)
    return {
        'publishers': publishers,
        'title': title,
        'journal': journal,
        'publication_year': publication_year,
    }
