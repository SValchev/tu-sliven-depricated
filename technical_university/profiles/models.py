import os
from datetime import datetime as dt

from django.conf import settings
from django.db import models
from django.db import transaction
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.http import Http404
from django.urls.base import reverse
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _
from transliterate import translit


################################
# Profile QuerySet and Manager #
################################

class ProfileQuerySet(models.QuerySet):
    def active(self):
        return self.filter(active=True)


class ProfileManager(models.Manager):
    def get_queryset(self):
        return ProfileQuerySet(self.model, using=self._db)

    def active(self):
        return self.get_queryset().active()


#######################
# Profile Class Model #
#######################

def cover_image_upload_location(instance, filename):
    return os.path.join('Profiles', instance.slug, filename)


class Profile(models.Model):
    class Meta:
        verbose_name = _('profile')
        verbose_name_plural = _('profiles')

    TEACHER = 1
    STUDENT_COUNCIL = 2
    ROLE = (
        (TEACHER, _('Teacher')),
        (STUDENT_COUNCIL, _('Student Council'))
    )

    slug = models.SlugField(unique=True, null=False, blank=True, verbose_name=_('slug'))
    first_name = models.CharField(max_length=100, verbose_name=_('first name'))
    second_name = models.CharField(max_length=100, verbose_name=_('second name'))
    last_name = models.CharField(max_length=100, verbose_name=_('last name'))
    image = models.ImageField(upload_to=cover_image_upload_location, verbose_name=_('image'))
    active = models.BooleanField(default=True, verbose_name=_('active'))
    description = models.TextField(verbose_name=_('description'))
    email = models.EmailField(blank=True, null=True, verbose_name=_('email'))
    phone_number = models.CharField(max_length=20, blank=True, null=True, verbose_name=_('phone number'))
    role = models.PositiveSmallIntegerField(choices=ROLE, verbose_name=_('role'))

    objects = ProfileManager()

    @property
    def get_first_last_name(self):
        return '{} {}'.format(self.first_name, self.last_name)

    @property
    def get_full_name(self):
        return "{} {} {}".format(self.first_name, self.second_name, self.last_name)

    @property
    def is_teacher(self):
        return self.role == self.TEACHER

    @property
    def is_student_council(self):
        return self.role == self.STUDENT_COUNCIL

    @property
    def have_sections(self):
        return self.is_teacher or self.is_student

    @property
    def get_title(self):
        if self.is_teacher:
            return self.teacher_profile.get_title_and_degree
        if self.is_student_council:
            return self.student_council_profile.get_title

    def get_edit_url(self):
        if self.role == self.TEACHER:
            return reverse('profile:teacher_edit', kwargs={'slug': self.slug})
        elif self.role == self.STUDENT_COUNCIL:
            return reverse('profile:student_council_edit', kwargs={'slug': self.slug})
        assert False, 'Something is wrong with the get edit page function in the profile.'

    def get_edit_publication_url(self):
        if self.role != self.TEACHER:
            raise ValueError('User profile is not a teacher.')
        return reverse('profile:teacher_publications_edit', kwargs={'slug': self.slug})

    def get_departament(self):
        try:
            return self.departamentprofile.departament
        except models.ObjectDoesNotExist:
            return None

    def get_absolute_url(self):
        if not self.active:
            raise Http404
        if self.is_teacher:
            return reverse('profile:teacher_detail', kwargs={'slug': self.slug})
        elif self.is_student_council:
            return reverse('profile:student_council_detail', kwargs={'slug': self.slug})

    def get_publication_edit_url(self):
        if not self.is_teacher:
            raise ValueError('Profile should be teacher profile')
        return reverse('profile:publication_edit', kwargs={'slug': self.slug})

    def get_password_change_url(self):
        return reverse('accounts:password-change', kwargs={'slug': self.slug})

    def get_school_subject_edit_url(self):
        return reverse('departament:school_subjects_list', kwargs={'slug': self.get_departament().slug})

    def get_cabinet(self):
        try:
            return self.teacher_profile.cabinet
        except models.ObjectDoesNotExist:
            return None

    def __str__(self):
        return "{} {} {}".format(self.first_name, self.second_name, self.last_name)


def _get_translated_name(instance):
    name = instance.get_first_last_name
    name = name.replace('ь', 'и')
    name = name.replace('ч', 'цх')
    return translit(name, reversed=True)


# TODO: Make sure that the slug is unique
@receiver(signal=pre_save, sender=Profile)
def slug_pre_save_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        name = _get_translated_name(instance)
        slug = slugify(name)
        instance.slug = slug


################################
# Abstract Staff Profile Model #
################################

class StaffProfileQuerySet(ProfileQuerySet):
    pass


class StaffProfileManager(ProfileManager):
    pass


class AbstractStaffProfile(models.Model):
    degree = models.CharField(max_length=225, default='')
    cabinet = models.CharField(max_length=4, default='0000')

    # start work
    # end work
    # start break
    # end break

    class Meta:
        abstract = True


############################
# Teach Profile Class Model#
############################


class TeacherProfileQuerySet(StaffProfileQuerySet):
    def habilitated(self, habilitated):
        return self.active().filter(habilitated=habilitated)


class TeachProfileManager(StaffProfileManager):
    def get_queryset(self):
        return TeacherProfileQuerySet(self.model, using=self._db)

    def habilitated(self, is_habilitated=True):
        return self.get_queryset().habilitated(is_habilitated)


class TeacherProfile(AbstractStaffProfile):
    class Meta:
        verbose_name = _('teacher profile')
        verbose_name_plural = _('teacher profiles')

    ASSISTANT = 1
    CHIEF_ASSISTANT = 2
    TEACHER = 3
    SENIOR_TEACHER = 4
    DOCENT = 5
    PROFESSOR = 6

    ACADEMIC_CHOICES = (
        (ASSISTANT, _('Assistant')),
        (CHIEF_ASSISTANT, _('Chief Assistant')),
        (TEACHER, _('Teacher')),
        (SENIOR_TEACHER, _('Senior Teacher')),
        (DOCENT, _('Docent')),
        (PROFESSOR, _('Professor'))
    )

    profile = models.OneToOneField(Profile, related_name='teacher_profile', on_delete=models.CASCADE,
                                   verbose_name=_('profile'))
    title = models.PositiveSmallIntegerField(choices=ACADEMIC_CHOICES, default=1, verbose_name=_('title'))
    habilitated = models.BooleanField(verbose_name=_('habilitated'))

    objects = TeachProfileManager()

    @property
    def get_title_and_degree(self):
        choices = dict(self.ACADEMIC_CHOICES)
        return "{} {}".format(choices[self.title], self.degree)

    def __str__(self):
        return self.profile.get_full_name


@receiver(signal=pre_save, sender=TeacherProfile)
def habilitated_pre_save_receiver(sender, instance, *args, **kwargs):
    is_habilitated = instance.title == TeacherProfile.PROFESSOR or instance.title == TeacherProfile.DOCENT
    instance.habilitated = is_habilitated


############################
# Publications class Model #
############################

# Get all possible years from now until 1980
YEAR_CHOICES = [(year, year) for year in range(dt.now().year, 1980, -1)]


class Publication(models.Model):
    class Meta:
        verbose_name = _('publication')
        verbose_name_plural = _('publications')
        ordering = ['-publication_year']

    teacher = models.ForeignKey(TeacherProfile, verbose_name=_('teacher'))
    publishers = models.CharField(max_length=225, verbose_name=_('publishers'))
    title = models.CharField(max_length=225, verbose_name=_('title'))
    journal = models.CharField(max_length=225, verbose_name=_('journal'))
    publication_year = models.PositiveIntegerField(choices=YEAR_CHOICES, verbose_name=_('publication year'))


########################
# Textbook class Model #
########################


class Textbook(models.Model):
    class Meta:
        verbose_name = _('textbook')
        verbose_name_plural = _('textbooks')
        ordering = ['-publication_year']

    teacher = models.ForeignKey(TeacherProfile, verbose_name=_('teacher'))
    publishers = models.CharField(max_length=225, verbose_name=_('publishers'))
    title = models.CharField(max_length=225, verbose_name=_('title'))
    publication_year = models.PositiveIntegerField(choices=YEAR_CHOICES, verbose_name=_('publication years'))


#######################################
# Student Council Profile Class Model #
#######################################


class StudentCouncilProfileQuerySet(ProfileQuerySet):
    def from_faculty(self, is_from_faculty):
        return self.active().filter(faculty=is_from_faculty)


class StudentCouncilProfileManager(ProfileManager):
    def get_queryset(self):
        return StudentCouncilProfileQuerySet(self.model, using=self._db)

    def from_faculty(self, is_from_faculty):
        return self.get_queryset().filter(faculty=is_from_faculty)


class StudentCouncilProfile(models.Model):
    class Meta:
        verbose_name = _('student council profile')
        verbose_name_plural = _('student council profiles')

    profile = models.OneToOneField(Profile, related_name='student_council_profile', verbose_name=_('profile'))
    title = models.CharField(max_length=225, default='', verbose_name=_('title'))
    facility = models.BooleanField(default=True, verbose_name=_('facility'))

    objects = StudentCouncilProfileManager

    @property
    def get_title(self):
        return self.title


##################################
# POST SAVE receiver for Profile #
##################################


@receiver(signal=post_save, sender=Profile)
def profile_post_save_receiver(sender, instance, created, *args, **kwargs):
    """
    Post save receiver for Profile.
    Place it aways after all models so you could create instance of them.
    """

    profile_classes = {
        Profile.TEACHER: TeacherProfile,
        Profile.STUDENT_COUNCIL: StudentCouncilProfile,
    }

    if not created:
        return None

    extend_profile_class = profile_classes.get(instance.role)
    assert extend_profile_class, 'There should be aways a profile class'

    with transaction.atomic():
        extend_profile_class.objects.create(profile=instance)
    return None


############################
# User Profile Class Model #
############################

class UserProfile(models.Model):
    class Meta:
        verbose_name = _('user profile')
        verbose_name_plural = _('user profiles')

    user = models.OneToOneField(settings.AUTH_USER_MODEL, verbose_name=_('user'))
    profile = models.OneToOneField(Profile, verbose_name=_('profile'))

    def get_absolute_url(self):
        return self.profile.get_absolute_url()

    def get_departament(self):
        try:
            return self.profile.departamentprofile.departament
        except models.FieldDoesNotExist:
            return None

    def __str__(self):
        return self.user.username