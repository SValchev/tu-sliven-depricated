from django.apps import AppConfig
from django.utils.text import ugettext_lazy as _


class ProfilesConfig(AppConfig):
    name = 'profiles'
    verbose_name = _('profiles')
