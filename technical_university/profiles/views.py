from django.contrib.auth.mixins import LoginRequiredMixin, AccessMixin
from django.http.response import Http404
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView

from profiles.forms import PublicationFormsetFactory, ProfileForm
from profiles.models import Profile
from technical_university.mixins import FormSetOwnerViewMixin


class TeacherProfileDetailView(DetailView):
    model = Profile
    template_name = 'profiles/type/teacher.html'

    def get_context_data(self, **kwargs):
        context = super(TeacherProfileDetailView, self).get_context_data(**kwargs)
        profile = self.get_object()
        context['departament'] = profile.departamentprofile.departament
        context['title'] = profile.teacher_profile.get_title_and_degree

        context['subjects'] = profile.teacher_profile.schoolsubject_set.all()
        context['publications'] = profile.teacher_profile.publication_set.all()
        context['books'] = profile.teacher_profile.textbook_set.all()

        return context


class TeacherProfileUpdateView(AccessMixin, UpdateView):
    model = Profile
    form_class = ProfileForm
    formset_class = None
    template_name = 'profiles/edits/teacher_profile_edit.html'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return self.handle_no_permission()
        if request.user.userprofile.profile.slug != kwargs.get('slug'):
            raise Http404
        return super(TeacherProfileUpdateView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return super(TeacherProfileUpdateView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(TeacherProfileUpdateView, self).get_context_data(**kwargs)
        context['departament'] = self.get_object().departamentprofile.departament
        return context


class PublicationUpdateView(AccessMixin, FormSetOwnerViewMixin):
    model = Profile
    template_name = 'profiles/edits/teacher_publication_edit.html'
    formset_class = PublicationFormsetFactory
    object = None
    owner = None
    formset_queryset_class = None

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return self.handle_no_permission()
        if request.user.userprofile.profile.slug != kwargs.get('slug'):
            raise Http404

        obj = self.get_object()
        self.object = obj
        self.owner = obj.teacher_profile
        return super(PublicationUpdateView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PublicationUpdateView, self).get_context_data(**kwargs)
        context['departament'] = self.request.user.userprofile.get_departament()
        return context

    def get_formset_queryset(self):
        teacher = self.get_owner()
        return teacher.publication_set.all()


#################################
# Student Council Profile Views #
#################################


class StudentCouncilProfileUpdateView(LoginRequiredMixin, UpdateView):
    model = Profile
    form_class = ProfileForm
    template_name = 'profiles/edits/teacher_profile_edit.html'

    def dispatch(self, request, *args, **kwargs):
        d = super(StudentCouncilProfileUpdateView, self).dispatch(request, *args, **kwargs)
        if request.user.userprofile.profile.slug != kwargs.get('slug'):
            raise Http404
        return d

    def get_context_data(self, **kwargs):
        context = super(StudentCouncilProfileUpdateView, self).get_context_data(**kwargs)
        context['departament'] = self.get_object().departamentprofile.departament
        return context


class StudentCouncilProfileDetailView(DetailView):
    model = Profile
    template_name = 'profiles/type/student-council.html'

    def get_context_data(self, **kwargs):
        context = super(StudentCouncilProfileDetailView, self).get_context_data(**kwargs)
        profile = self.get_object()
        context['departament'] = profile.departamentprofile.departament

        context['title'] = profile.student_council_profile.title

        return context
