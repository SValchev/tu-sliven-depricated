from django import forms
from django.forms import modelformset_factory, BaseModelFormSet

from profiles.models import Profile, Publication
from technical_university.mixins import OwnerModelFormSetMixin


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('first_name', 'second_name', 'last_name', 'image', 'description', 'email', 'phone_number')
        labels = {
            'first_name': 'Име',
            'second_name': 'Презиме',
            'last_name': 'Фамилия',
            'description': 'Описание',
            'email': 'Имейл',
            'phone_number': 'Телефон за конакти'
        }


class PublicationModelForm(forms.ModelForm):
    class Meta:
        model = Publication
        fields = ('publishers', 'title', 'journal', 'publication_year')

    def save(self, commit=True, owner=None):
        instance = super(PublicationModelForm, self).save(commit=False)
        if commit:
            instance.teacher = owner
            instance.save()
        return instance

class PublicationFormSet(OwnerModelFormSetMixin, BaseModelFormSet):
    pass

PublicationFormsetFactory = modelformset_factory(
    Publication,
    PublicationModelForm,
    formset=PublicationFormSet,
    extra=2,
    can_delete=True,
)
