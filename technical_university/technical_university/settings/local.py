from .base import *

SECRET_KEY = '^ly%!2n*du(51zr7bpkup0p)a4bp44wm+7@$y7#16@l*hkt-^^'

DEBUG = True
DEBUG_TOOLBAR_PATCH_SETTINGS = True

INSTALLED_APPS +=[
    'debug_toolbar',
    'django_extensions',
]

# Database
# https://docs.djangoproject.com/en/1.~10/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'tech_uni',
        'USER': 'svalchev',
        'PASSWORD': '',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

# This is where your static files live
# Here is whre you should import files
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]

# Where static files will be collected by django
# Do not put anything inside it
STATIC_ROOT = os.path.join(PROJECT_DIR, "static")

MEDIA_ROOT = os.path.join(PROJECT_DIR, "media")
