import os
from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'tech_uni',
        'USER': 'svalchev',
        'PASSWORD': 's3c737p@55w0rD',
        'HOST': 'db',
        'PORT': '5432',
    }
}

# This is where your static files live
# Here is whre you should import files
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]

# Where static files will be collected by django
# Do not put anything inside it
STATIC_ROOT = os.path.join(PROJECT_DIR, "static")

MEDIA_ROOT = os.path.join(PROJECT_DIR, "media")
