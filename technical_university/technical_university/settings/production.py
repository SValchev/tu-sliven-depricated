from .base import *

DEBUG = False

ALLOWED_HOSTS = ['*']

# Database
# https://docs.djangoproject.com/en/1.~10/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'tech_uni',
        'USER': 'svalchev',
        'PASSWORD': 's3c737p@55w0rD',
        'HOST': 'db',
        'PORT': '5432',
    }
}

STATIC_ROOT = os.path.join(PROJECT_DIR, "static")

MEDIA_ROOT = os.path.join(PROJECT_DIR, "media")
