from django.contrib.auth.mixins import AccessMixin
from django.http import Http404
from django.shortcuts import redirect
from django.views.generic.base import TemplateResponseMixin, View
from django.views.generic.detail import SingleObjectMixin


class OwnerModelFormSetMixin(object):
    """
    Works combined with publication model form
    """
    owner = None

    def set_owner(self, owner):
        self.owner = owner

    def get_owner(self):
        if not self.owner:
            raise ValueError('Owner should be set for {}'.format(self.__class__.__name__))
        return self.owner

    def save_new(self, form, commit=True):
        return form.save(commit=commit, owner=self.get_owner())

    def save_existing(self, form, instance, commit=True):
        """Saves and returns an existing model instance for the given form."""
        return form.save(commit=commit, owner=self.get_owner())


class FormSetOwnerViewMixin(TemplateResponseMixin, SingleObjectMixin, View):
    template_name = None
    formset_class = None
    formset_queryset = None
    object = None
    owner = None

    def get(self, request, *args, **kwargs):
        context = super(FormSetOwnerViewMixin, self).get_context_data(**kwargs)
        context['formset'] = self.get_formset()
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        formset = self.get_formset(data=request.POST)
        if formset.is_valid():
            return self.formset_valid(formset)
        return self.formset_invalid(formset)

    def formset_valid(self, formset):
        formset.save()
        return redirect(self.object)

    def formset_invalid(self, formset):
        context = self.get_context_data()
        context['formset'] = formset
        return self.render_to_response(context)

    def get_formset_class(self):
        if not self.formset_class:
            raise ValueError('Missing formset class at {}'.format(self.__class__.__name__))
        return self.formset_class

    def get_formset(self, data=None, initial=None):
        formset_class = self.get_formset_class()

        owner = self.get_owner()
        formset_queryset = self.get_formset_queryset()
        formset = formset_class(data=data, initial=initial, queryset=formset_queryset)
        formset.set_owner(owner)
        return formset

    def get_template(self):
        template = self.template_name
        if not template:
            raise ValueError('Missing template name at {}'.format(self.__class__.__name__))
        return template

    def get_owner(self):
        if not self.owner:
            raise ValueError('Missing owner name at {}'.format(self.__class__.__name__))
        return self.owner

    def get_formset_queryset(self):
        raise NotImplementedError('Not implemented method at {}'.format(self.__class__.__name__))


class StaffCurrentDepartamentUserLoginRequiredMixin(AccessMixin):
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return self.handle_no_permission()

        # User must be with teacher profile
        if not request.user.userprofile.profile.is_teacher:
            raise Http404()

        # User Must be staff
        if not request.user.is_staff:
            raise Http404()

        # User must be from the same departament
        user_departament = request.user.userprofile.get_departament()
        if user_departament.slug != kwargs.get('slug'):
            raise Http404()

        return super(StaffCurrentDepartamentUserLoginRequiredMixin, self).dispatch(request, *args, **kwargs)
