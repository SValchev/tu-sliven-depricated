from django.apps import AppConfig
from django.utils.text import ugettext_lazy as _


class DepartmentsConfig(AppConfig):
    name = 'departments'
    verbose_name = _('departaments')
