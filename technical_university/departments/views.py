from django.contrib.auth.mixins import AccessMixin
from django.http import Http404
from django.shortcuts import redirect, get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import CreateView
from django.views.generic import DetailView, UpdateView
from django.views.generic import FormView
from django.views.generic import ListView

from departments.forms import DepartamentModelForm, SchoolSubjectListForm, SchoolSubjectModelForm
from news.models import News
from profiles.models import Profile
from technical_university.mixins import StaffCurrentDepartamentUserLoginRequiredMixin
from .models import Departament, SchoolSubject


class HomeView(DetailView):
    model = Departament
    template_name = 'departments/home.html'


class HomeEditView(AccessMixin, UpdateView):
    model = Departament
    form_class = DepartamentModelForm
    template_name = 'departments/edits/home_edit.html'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return self.handle_no_permission()
        if not request.user.is_staff:
            raise Http404
        if request.user.userprofile.profile.departamentprofile.departament.slug != self.kwargs.get('slug'):
            raise Http404
        return super(HomeEditView, self).dispatch(request, *args, **kwargs)


class HistoryView(DetailView):
    model = Departament
    template_name = 'departments/history.html'
    title_name = 'History'


class LaboratoriesView(DetailView):
    model = Departament
    template_name = 'departments/laboratory.html'


class StructureView(DetailView):
    model = Departament
    models = Profile
    template_name = 'departments/structure/_section_structure.html'
    object = None

    def get_context_data(self, **kwargs):
        context = super(StructureView, self).get_context_data(**kwargs)

        context['non_existing_message'] = 'Не същестуват участници от дадената графа'

        departament = self.get_object()
        context['departament'] = departament

        leader = departament.departamentleader.leader
        context['leader'] = leader

        profiles_ids = departament.departamentprofile_set.all().exclude(profile=leader).values_list('pk', flat=True)
        profile_qs = Profile.objects.filter(id__in=profiles_ids)
        if leader.is_teacher:
            self._get_teachers_profiles(context, profile_qs)

        elif leader.is_student_council:
            self._get_student_council_profiles(context, profile_qs)
        else:
            context['profiles'] = profile_qs

        return context

    @staticmethod
    def _get_teachers_profiles(context, profile_qs=None):
        context['is_teacher_department'] = True

        context['left_group'] = 'Нехабилитирани'
        context['right_group'] = 'Хабилитирани'
        if profile_qs:
            context['left_group_participants'] = profile_qs.filter(teacher_profile__habilitated=True)
            context['right_group_participants'] = profile_qs.filter(teacher_profile__habilitated=False)

    @staticmethod
    def _get_student_council_profiles(context, profile_qs=None):
        context['is_student_council_department'] = True

        context['left_group'] = 'Факултет'
        context['right_group'] = 'Колеж'

        if profile_qs:
            context['left_group_participants'] = profile_qs(student_council_profile__from_faculty=True)
            context['right_group_participants'] = profile_qs(student_council_profile__from_faculty=False)


class SchoolSubjectListEditView(StaffCurrentDepartamentUserLoginRequiredMixin,
                                ListView):
    ACTION_DELETE = 'delete'

    template_name = 'departments/edits/school_subjects_edit_list.html'
    model = SchoolSubject
    paginate_by = 10
    form_class = SchoolSubjectListForm

    def post(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        form = self.form_class(self.get_queryset(), data=request.POST)
        if form.is_valid():
            return self.form_valid(form)
        return self.form_invalid(form)

    def form_valid(self, form):
        form.cleaned_data['school_subject'].delete()
        return redirect(reverse_lazy('departament:school_subjects_list', kwargs={'slug': self.kwargs['slug']}))

    def form_invalid(self, form):
        context = self.get_context_data()
        context['form'] = form
        return self.render_to_response(context)

    def get_queryset(self):
        queryset = super(SchoolSubjectListEditView, self).get_queryset()
        return queryset.filter(departament__slug=self.kwargs.get('slug'))

    def get_context_data(self, **kwargs):
        context = super(SchoolSubjectListEditView, self).get_context_data(**kwargs)
        context['departament'] = get_object_or_404(Departament, slug=self.kwargs.get('slug'))

        return context


class SchoolSubjectCreateView(StaffCurrentDepartamentUserLoginRequiredMixin,
                              FormView):
    template_name = 'departments/edits/school_subject.html'
    form_class = SchoolSubjectModelForm

    def get_context_data(self, **kwargs):
        context = super(SchoolSubjectCreateView, self).get_context_data(**kwargs)
        context['departament'] = get_object_or_404(Departament, slug=self.kwargs.get('slug'))
        return context

    def form_valid(self, form):
        form.save()
        return redirect(
            reverse_lazy(
                'departament:school_subjects_list', kwargs={'slug': self.kwargs.get('dep_slug')})
        )


class SchoolSubjectUpdateView(StaffCurrentDepartamentUserLoginRequiredMixin,
                              UpdateView):
    template_name = 'departments/edits/school_subject.html'
    model = SchoolSubject
    form_class = SchoolSubjectModelForm

    def get_success_url(self):
        return reverse_lazy(
            'departament:school_subjects_list',
            kwargs={'slug': self.kwargs.get('slug')}
        )


class NewsListView(ListView):
    model = News
    template_name = 'departments/news/news_list.html'
    slug_field = 'slug'

    def get_queryset(self):
        qs = super(NewsListView, self).get_queryset()
        return qs.filter(departamentnews__departament__slug=self.get_slug_field())

    def get_context_data(self, **kwargs):
        context = super(NewsListView, self).get_context_data(**kwargs)
        context['departament'] = get_object_or_404(Departament, slug=self.get_slug_field())
        return context

    def get_slug_field(self):
        return self.kwargs.get(self.slug_field)


class NewsListEditView(object):
    pass


class NewsDetailView(DetailView):
    pass


class NewsCreateView(CreateView):
    pass


class NewsUpdateView(UpdateView):
    pass
