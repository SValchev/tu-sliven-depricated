from django import forms
from django.utils.translation import ugettext_lazy as _

from departments.models import Departament, SchoolSubject


class DepartamentModelForm(forms.ModelForm):
    class Meta:
        model = Departament
        fields = ('content', 'struct_info', 'cover_image')
        labels = {
            'content': 'Departament info',
            'struct_info': _('Departament structure info'),
        }


class SchoolSubjectModelForm(forms.ModelForm):
    class Meta:
        model = SchoolSubject
        fields = ('title', 'description', 'teachers',)
        labels = {
            'title': _('School subject name'),
            'description': _('Description'),
            'teachers': _('Teachers'),
        }


class SchoolSubjectListForm(forms.Form):
    school_subject = forms.ModelChoiceField(queryset=SchoolSubject.objects.none())

    def __init__(self, school_subject_qs, **kwargs):
        super(SchoolSubjectListForm, self).__init__(**kwargs)
        self.fields['school_subject'].queryset = school_subject_qs
