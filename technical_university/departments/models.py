import os
from transliterate import translit

from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django.urls.base import reverse
from django.utils.text import slugify

from news.models import News
from profiles.models import Profile, TeacherProfile


def cover_image_upload_location(instance, filename):
    save_url = os.path.join(instance.slug, filename)
    return save_url


class Departament(models.Model):
    class Meta:
        verbose_name = _('departament')
        verbose_name_plural = _('departaments')

    slug = models.SlugField(unique=True, null=False, blank=True, verbose_name=_('slug'))
    title = models.CharField(max_length=150, verbose_name=_('title'))
    content = models.TextField(verbose_name=_('content'))
    struct_info = models.TextField(blank=True, default='', verbose_name=_('structure info'))
    cover_image = models.ImageField(upload_to=cover_image_upload_location, verbose_name=_('cover image'))

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('departament:home', kwargs={'slug': self.slug})

    def get_structure_url(self):
        return reverse('departament:structure', kwargs={'slug': self.slug})

    def get_laboratories_url(self):
        return reverse('departament:laboratories', kwargs={'slug': self.slug})

    def get_history_url(self):
        return reverse('departament:history', kwargs={'slug': self.slug})

    def get_edit_url(self):
        return reverse('departament:home_edit', kwargs={'slug': self.slug})

    @property
    def is_teachers_department(self):
        try:
            return self.departamentleader.leader.is_teacher
        except models.FieldDoesNotExist:
            return False


# TODO: Make sure that the slug is unique
@receiver(signal=pre_save, sender=Departament)
def department_slug_receiver(sender, instance, *args, **kwargs):
    title_split = instance.title.split()
    initials = [word[0] for word in title_split if len(word) > 1]
    initials = "".join(initials)
    slug = slugify(translit(initials, reversed=True))
    instance.slug = slug


class DepartamentLeader(models.Model):
    class Meta:
        verbose_name = _('departament leader')
        verbose_name_plural = _('departament leaders')

    departament = models.OneToOneField(Departament, verbose_name=_('departament'))
    leader = models.OneToOneField(Profile, verbose_name=_('leader'))

    def __str__(self):
        return "{}: {}".format(self.departament.title, self.leader.get_full_name)


class DepartamentProfile(models.Model):
    class Meta:
        verbose_name = _('departament profile')
        verbose_name_plural = _('departament profiles')

    departament = models.ForeignKey(Departament, verbose_name=_('departament'))
    profile = models.OneToOneField(Profile, verbose_name=_('profile'))

    def __str__(self):
        return self.profile.get_full_name


class SchoolSubject(models.Model):
    class Meta:
        verbose_name = _('school subject')
        verbose_name_plural = _('school subjects')

    departament = models.ForeignKey(Departament, verbose_name=_('departament'))
    teachers = models.ManyToManyField(TeacherProfile, verbose_name=_('teachers'))
    title = models.CharField(max_length=225, verbose_name=_('title'))
    description = models.TextField(verbose_name=_('description'))


class Laboratory(models.Model):
    class Meta:
        verbose_name = _('Laboratory')
        verbose_name_plural = _('Laboratories')

    department = models.ForeignKey(Departament, verbose_name=_('departament'))
    room_number = models.CharField(max_length=4, verbose_name=_('room number'))
    subject_name = models.CharField(max_length=225, verbose_name=_('subject name'))


class LaboratoryImage(models.Model):
    class Meta:
        verbose_name = _('laboratory image')
        verbose_name_plural = _('laboratory images')

    laboratory = models.ForeignKey(Laboratory, verbose_name=_('laboratory'))
    image = models.ImageField(verbose_name=_('image'))


class DepartamentNews(models.Model):
    departament = models.OneToOneField(Departament)
    news = models.OneToOneField(News)

    def __str__(self):
        return '{}: {}'.format(self.department.title, self.news.title)
