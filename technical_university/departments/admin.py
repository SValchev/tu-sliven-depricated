from django.contrib import admin

from departments.models import Laboratory, LaboratoryImage
from .models import Departament, SchoolSubject, DepartamentLeader, DepartamentProfile


class SchoolSubjectTabularInline(admin.TabularInline):
    model = SchoolSubject
    fields = ('title', 'description', 'teachers')
    extra = 1


class DepartmentAdmin(admin.ModelAdmin):
    inlines = [
        SchoolSubjectTabularInline,
    ]


class LaboratoryImageTabularInline(admin.TabularInline):
    model = LaboratoryImage
    max_num = 2


class LaboratoryAdmin(admin.ModelAdmin):
    inlines = [
        LaboratoryImageTabularInline,
    ]


admin.site.register(Departament, DepartmentAdmin)
admin.site.register(Laboratory, LaboratoryAdmin)
admin.site.register(DepartamentLeader)
admin.site.register(DepartamentProfile)
