from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        regex=r'^(?P<slug>[\w-]+)/$',
        view=views.HomeView.as_view(),
        name='home',
    ),
    url(
        regex=r'^(?P<slug>[\w-]+)/edit/$',
        view=views.HomeEditView.as_view(),
        name='home_edit',
    ),
    url(
        regex=r'^(?P<slug>[\w-]+)/structure/$',
        view=views.StructureView.as_view(),
        name='structure'
    ),
    url(
        regex=r'(?P<slug>[\w-]+)/history/$',
        view=views.HistoryView.as_view(),
        name='history'
    ),
    url(
        regex=r'(?P<slug>[\w-]+)/laboratories/$',
        view=views.LaboratoriesView.as_view(),
        name='laboratories'
    ),
    url(
        regex=r'(?P<slug>[\w-]+)/school-subjects/list/$',
        view=views.SchoolSubjectListEditView.as_view(),
        name='school_subjects_list'
    ),
    url(
        regex=r'(?P<slug>[\w-]+)/school-subject/create/$',
        view=views.SchoolSubjectCreateView.as_view(),
        name='school_subjects_create'
    ),

    url(
        regex=r'(?P<slug>[\w-]+)/school-subject/(?P<pk>\d+)/edit/$',
        view=views.SchoolSubjectUpdateView.as_view(),
        name='school_subjects_edit'
    ),
    url(
        regex=r'(?P<slug>[\w-]+)/news/$',
        view=views.NewsListView.as_view(),
        name='news_list'
    )
]


