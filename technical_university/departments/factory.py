from faker import Faker


def get_school_subject_data():
    faker = Faker()
    title = faker.text(max_nb_chars=225)
    description = faker.text(max_nb_chars=500)
    return {
        'title': title,
        'description': description
    }
