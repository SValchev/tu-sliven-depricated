# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-11-12 17:56
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('departments', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='departament',
            options={'verbose_name': 'departament', 'verbose_name_plural': 'departaments'},
        ),
        migrations.AlterModelOptions(
            name='departamentleader',
            options={'verbose_name': 'departament leader', 'verbose_name_plural': 'departament leaders'},
        ),
        migrations.AlterModelOptions(
            name='departamentprofile',
            options={'verbose_name': 'departament profile', 'verbose_name_plural': 'departament profiles'},
        ),
        migrations.AlterModelOptions(
            name='laboratoryimage',
            options={'verbose_name': 'laboratory image', 'verbose_name_plural': 'laboratory images'},
        ),
        migrations.AlterModelOptions(
            name='schoolsubject',
            options={'verbose_name': 'school subject', 'verbose_name_plural': 'school subjects'},
        ),
        migrations.RenameField(
            model_name='schoolsubject',
            old_name='department',
            new_name='departament',
        ),
    ]
