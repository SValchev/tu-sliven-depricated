from django.template import Library

register = Library()


@register.simple_tag
def pagin_row_calc(page, item_per_page, row):
    page = page or 1
    return (int(page) * int(item_per_page)) + int(row)
