from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        regex=r'^login/$',
        view=views.LoginView.as_view(),
        name='login',
    ),
    url(
        regex=r'^(?P<slug>[\w-]+)/password-change/$',
        view=views.PasswordChangeView.as_view(),
        name='password-change',
    ),
]
