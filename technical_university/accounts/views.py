from django.contrib.auth import login
from django.contrib.auth.forms import AuthenticationForm, PasswordChangeForm
from django.shortcuts import redirect
from django.views.generic import DetailView
from django.views.generic import TemplateView
from django.contrib.auth import update_session_auth_hash

from profiles.models import Profile


class LoginView(TemplateView):
    template_name = 'accounts/login.html'

    def post(self, request, *args, **kwargs):
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            return self.form_valid(form)
        return self.form_invalid(form)

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return redirect(request.user.userprofile.get_absolute_url())
        kwargs['form'] = AuthenticationForm()
        return super(LoginView, self).get(request, *args, **kwargs)

    def form_invalid(self, form):
        return self.render_to_response({'form': form})

    def form_valid(self, form):
        user = form.get_user()
        login(self.request, user)
        return redirect(user.userprofile.get_absolute_url())


class PasswordChangeView(DetailView):
    model = Profile
    template_name = 'accounts/password_change.html'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(form=PasswordChangeForm(request.user), object=self.object)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = PasswordChangeForm(request.user, data=request.POST)
        if form.is_valid():
            return self.form_valid(form)
        return self.form_invalid(form)

    def form_valid(self, form):
        form.save()
        update_session_auth_hash(self.request, form.user)
        return redirect('accounts:login')

    def form_invalid(self, form):
        context = self.get_context_data()
        context['form'] = form
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(PasswordChangeView, self).get_context_data(**kwargs)
        context['departament'] = self.object.get_departament()
        return context
